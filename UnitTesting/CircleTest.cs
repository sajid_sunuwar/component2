﻿using Component2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class CircleTest
    {
        [TestMethod]
        public void TestMethod3()
        {
            Circle circle = new Circle();
            circle.set(System.Drawing.Color.AliceBlue, true, 0, 0, 100);

            int[] expect = { 100 };
            int[] result = { circle.radius};

            Assert.IsTrue(expect[0] == result[0]);
        }
    }
}