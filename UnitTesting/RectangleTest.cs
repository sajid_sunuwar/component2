﻿using Component2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class RectangleTest
    {
        [TestMethod]
        public void TestMethod2()
        {
            Rectangle rect = new Rectangle();
            rect.set(System.Drawing.Color.AliceBlue, true, 0, 0, 200, 200);

            int[] expect = {200,200};
            int[] result = { rect.width, rect.height };

            Assert.IsTrue(expect[0] == result[0]);
            Assert.IsTrue(expect[1] == result[1]);
        }
    }
}
