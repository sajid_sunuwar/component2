using Component2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class TriangleTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Triangle triangle = new Triangle();
            triangle.set(System.Drawing.Color.AliceBlue, true, 0, 0, 50, 50);

            int[] expect = {50,50};
            int[] result = {triangle.width, triangle.height};

            Assert.IsTrue(expect[0] == result[0]);
            Assert.IsTrue(expect[1] == result[1]);
        }
    }
}
