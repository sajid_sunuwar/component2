﻿using Component2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class ShapeFactoryTest
    {
        [TestMethod]
        public void TestMethod4()
        {
            ShapeFactory shape = new ShapeFactory();
            Shape rectangle = shape.getShape("RECTANGLE");
            Shape circle = shape.getShape("CIRCLE");

            Assert.IsTrue(rectangle is Rectangle);
            Assert.IsTrue(circle is Circle);
        }
    }
}