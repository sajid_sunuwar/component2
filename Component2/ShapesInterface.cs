﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2
{
    /// <summary>
    /// Interface class which is inherited by child classes.
    /// </summary>
    public interface Shapes
    {
        /// <summary>
        /// <c>set</c> sets the parameters of a shape.
        /// </summary>
        /// <param name="color">Colour of shape</param>
        /// <param name="fill">Boolean to fill shape</param>
        /// <param name="list">List of additional parameters</param>
        void set(Color color, bool fill, params int[] list);

        /// <summary>
        /// <c>draw</c> draws/paints shapes.
        /// </summary>
        /// <param name="g">Graphics object</param>
        void draw(Graphics g);
    }
}
