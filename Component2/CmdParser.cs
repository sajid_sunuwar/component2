﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Component2
{
    /// <summary>
    /// Class which parses commands
    /// </summary>
    public class CmdParser
    {
        Dictionary<String, String> variables = new Dictionary<string, string>();
        int[,] loopCount;        //number of loop and its starting and ending line (basically stores the desired line number)
        String[] loopCondition;    //stores the condition of the loop according to index order of recorder
        Dictionary<String, String> methodParameters = new Dictionary<string, string>();
        ArrayList newMethod;
        DataTable data;   //compute expressions
        bool ifLoop, whileLoop, methodLoop;      //used to stop parsing if condition is false during iteration
        int totalLines;
        Dictionary<ArrayList, Dictionary<String, String>> methods;  //stores Index, dictionary stores name of method and its variable 
        int[,] methodRecorder;        //number of method and its starting and ending line (basically stores the desired line number)
        int methodCount;            //number of methods
        RichTextBox rtb;
        StringBuilder errors;
        Canvas displayCanvas;
        
        /// <summary>
        /// Construtor of CmdParser class
        /// </summary>
        public CmdParser()
        {
            loopCondition = new String[10];        //stores the condition of the loop according to index order of recorder
            ifLoop = false;
            whileLoop = false;
            methodLoop = false;
            data = new DataTable();
            this.variables = new Dictionary<String, String>();
            methods = new Dictionary<ArrayList, Dictionary<String, String>>();
            this.methodCount = 0;
            this.methodRecorder = new int[10, 2];         //number of loop and its starting and ending line
            this.loopCount = new int[10, 2];         //number of loop and its starting and ending line
            for (int i = 0; i < 10; i++)
            {
                this.loopCount[i, 0] = 0;
                this.loopCount[i, 1] = 0;
            }
        }

        /// <summary>
        /// It takes a string and computes it with the filter criteria
        /// </summary>
        /// <param name="expression">The expression to compute</param>
        /// <returns>An object set to the result of the computation</returns>
        public object computeExpression(String expression)
        {
            return this.data.Compute(expression, "");
        }


        /// <summary>
        /// Swaps keys that matches the expression
        /// </summary>
        /// <param name="expression">The expression to compute</param>
        /// <param name="varDict">Dictionary of key-value string-string</param>
        /// <returns></returns>
        public String swapVarValuesInExpression(String expression, Dictionary<String, String> varDict)
        {
            foreach (String key in varDict.Keys)
            {
                expression = Regex.Replace(expression, @"\b" + key + @"\b", this.variables[key]);
            }
            return expression;
        }


        /// <summary>
        /// Parses the commands using the preParser method
        /// </summary>
        /// <param name="rtb">Object if RichTextBox</param>
        /// <param name="displayCanvas">Object of Canvas</param>
        /// <param name="errors">Object of StringBuilder</param>
        public void parseCommand(RichTextBox rtb, Canvas displayCanvas, StringBuilder errors)
        {
            data = new DataTable();
            variables = new Dictionary<String, String>();
            this.totalLines = rtb.Lines.Length;
            this.errors = errors;
            this.displayCanvas = displayCanvas;
            this.rtb = rtb;

            for (int i = 1; i <= this.totalLines; i++)
            {
                preParser(i);
            }
        }

        /// <summary>
        /// Checks for commands first and if loop is encountered it is
        /// passed to runCommand method
        /// </summary>
        /// <param name="parameters">Lines of command</param>
        public void preParser(params object[] parameters)
        {
            int lineNo = Convert.ToInt32(parameters[0]);
            String line;
            if (parameters.Length > 1)
                line = parameters[1].ToString();
            else
                line = rtb.Lines[lineNo - 1].Trim().ToLower();

            if (line.Equals(""))
                return;
            if (this.methodLoop && (!line.Equals("endmethod")))
                return;
            if (this.whileLoop && (!line.Equals("endloop")))
                return;         
            if (this.ifLoop && (!line.Equals("endif")))
                return;

            String command = line.Split(' ')[0].Trim();

            if (command.Equals("circle") || command.Equals("rectangle") || command.Equals("triangle") || command.Equals("polygon")
                 || command.Equals("clear") || command.Equals("fill") || command.Equals("pen") || command.Equals("moveto")
                 || command.Equals("drawto") || command.Equals("reset") || command.Equals("run"))
            {
                parseLine(this.errors, this.displayCanvas, line, lineNo);
                return;
            }
            runCommand(line, lineNo);
        }


        /// <summary>
        /// Checks if line contains loop
        /// </summary>
        /// <param name="line">command string</param>
        /// <param name="lineNo">line at which the string is</param>
        public void runCommand(String line, int lineNo)
        {
            String command = line.Split(' ')[0].ToLower().Trim();
            switch (command)
            {
                case "if":
                    try
                    {
                        bool result = (bool)computeExpression(swapVarValuesInExpression(line.Split(' ')[1], this.variables));
                        if (result) 
                        {
                            //do nothing 
                        }
                        else 
                        { 
                            this.ifLoop = true; 
                        }
                    }
                    catch (Exception)
                    {
                        this.errors.Append("Invalid if syntax or wrong conditional expression at " + lineNo);
                    }
                    break;

                case "endif":
                    this.ifLoop = false;
                    break;

                case "while":
                    whileLoop = true;
                    for (int i = 0; i < 10; i++)
                    {
                        if (this.loopCount[i, 0] == 0)
                        {
                            this.loopCount[i, 0] = lineNo + 1;     //set starting line num of loop block
                            this.loopCondition[i] = line.Split(' ')[1]; //set loop condition
                            break;
                        }
                    }
                    break;

                case "endloop":
                    whileLoop = false;
                    for (int i = 0; i < 10; i++)
                    {
                        if (this.loopCount[i, 0] != 0)
                        {
                            this.loopCount[i, 1] = lineNo - 1;   //set last line num of loop block
                            try
                            {
                                bool result = (bool)computeExpression(swapVarValuesInExpression(this.loopCondition[i], this.variables));//computing condition of loop
                                while (result)
                                {
                                    for (int lineNum = this.loopCount[i, 0]; lineNum <= this.loopCount[i, 1]; lineNum++)
                                    {
                                        preParser(lineNum);
                                    }
                                    result = (bool)computeExpression(swapVarValuesInExpression(this.loopCondition[i], this.variables));
                                }
                                this.loopCount[i, 0] = 0;
                                this.loopCount[i, 1] = 0;
                                break;
                            }
                            catch (Exception)
                            {
                                this.errors.Append("Invalid while syntax or wrong conditional expression at" + lineNo);
                            }
                            break;
                        }
                    }
                    break;

                case "method":
                    methodLoop = true;
                    line = line.Substring(6, line.Length - 6).Trim();  // Removes "method" from line
                    String methodName = line.Split('(')[0].Trim();
                    String parameters = line.Split('(')[1].Trim(')');
                    newMethod = new ArrayList();
                    this.methodParameters = new Dictionary<String, String>();
                    newMethod.Add(methodCount);
                    newMethod.Add(methodName);

                    foreach (String par in parameters.Split(','))
                    {
                        if (!par.Trim().Equals(""))
                        {
                            this.methodParameters.Add(par.Trim(), "");      //parameter name with empty value for now
                        }    
                    }
                    methods.Add(newMethod, this.methodParameters); //Add method to dictionary   
                    methodRecorder[methodCount, 0] = lineNo + 1; //record its starting line with matching index(methodCount) num of method
                    break;

                case "endmethod":
                    methodLoop = false;
                    methodRecorder[methodCount, 1] = lineNo - 1;    //record ending line for a unique method count
                    methodCount++;
                    break;
                default:    //check for variable or method name
                    //check for variable name
                    if (line.Split('=').Length == 2)
                    {
                        String varName = line.Split('=')[0].Trim();
                        String rhs = line.Split('=')[1].Trim();                      //right hand side expression or a number 
                        rhs = swapVarValuesInExpression(rhs, this.variables);        //replace variable values in expression
                        int rhsResult = 0;
                        try
                        {
                            rhsResult = Convert.ToInt32(computeExpression(rhs));
                        }
                        catch (Exception)
                        {
                            this.errors.Append("Error in computing expression at line " + lineNo);
                        }
                        rhs = rhsResult.ToString();

                        if (this.variables.ContainsKey(varName))  //Reassign existing variable
                        {
                            this.variables[varName] = rhs;
                        } 
                        else                                // Create/Store new variable
                        {
                            this.variables.Add(varName, rhs);
                            return;
                        }       
                    }

                    else if (line.Split('(').Length == 2)           //check for method call
                    {
                        String calledMethodName = line.Split('(')[0].Trim();
                        bool match = false;
                        int uniqueMethodId, startLine = 0, endLine = 0;
                        foreach (ArrayList key1 in methods.Keys)
                        {
                            if (key1[1].Equals(calledMethodName))     //check if method name matches stored methods
                            {
                                match = true;
                                uniqueMethodId = Convert.ToInt32(key1[0]);          //unique method id of methodcount
                                this.methodParameters = methods[key1];              //parameters dictionary
                                startLine = methodRecorder[uniqueMethodId, 0];      //start line of method
                                endLine = methodRecorder[uniqueMethodId, 1];        //end line of method
                                break;
                            }
                            if (match)
                            {
                                break;
                            }
                        }
                        if (match)       //the name of the method matched
                        {
                            String mCallParameters = line.Trim(')').Split('(')[1];    //stores the parameters in array
                            String[] callParameters = mCallParameters.Split(',');
                            if (this.methodParameters.Count == 0)
                            {
                                if (callParameters.Length == 1 && callParameters[0].Equals(""))
                                { }  //do nothing 
                                else
                                {
                                    this.errors.Append("Number of parameters mismatched in method call and definition at " + lineNo);
                                    return;
                                }
                            }
                            else if (this.methodParameters.Count == callParameters.Length)
                            {
                                //put the values from called parameters to method parameters
                                String[] tempParams = new string[this.methodParameters.Count];
                                int count = 0;
                                foreach (String key in this.methodParameters.Keys)
                                {
                                    tempParams[count] = key;
                                    count++;
                                }
                                int noOfParameters = this.methodParameters.Count;
                                for (int i = 0; i < noOfParameters; i++)
                                {
                                    this.methodParameters[tempParams[i]] = callParameters[i].Trim();
                                }
                            }
                            else
                            {
                                this.errors.Append("Parameter number mismatch in method call");
                                return;
                            }
                            // run the method statements
                            if (this.methodParameters.Count > 0)
                            {
                                String methodLine = "";
                                for (int i = startLine; i <= endLine; i++)
                                {
                                    methodLine = rtb.Lines[i - 1];
                                    methodLine = swapVarValuesInExpression(methodLine, this.methodParameters);
                                    preParser(i, methodLine);
                                }
                            }
                            else
                            {
                                String methodLine = "";
                                for (int i = startLine; i <= endLine; i++)
                                {
                                    methodLine = rtb.Lines[i - 1];
                                    preParser(i, methodLine);
                                }
                            }
                        }
                        else
                        {
                            this.errors.Append("\r\n Invalid command at line " + lineNo);
                        }
                    }
                    else
                    {
                        this.errors.Append("\r\n Invalid command at line " + lineNo);
                        return;
                    }
                    break;
            }
        }




        
        /// <summary>
        /// Parses the line and calls the appropriate method if valid else records error 
        /// </summary>
        /// <param name="errorList">Object of StringBuider</param>
        /// <param name="displayCanvas">Object of Canvas</param>
        /// <param name="line">Command line</param>
        /// <param name="lineNo">Line at which command is</param>
        public void parseLine(StringBuilder errorList, Canvas displayCanvas, String line, int lineNo)
        {

            line = swapVarValuesInExpression(line, this.variables);
            if (line.Equals(""))        
                return;
            line = line.Trim().ToLower();              
            String[] cmd = line.Split(' ');           



            if (cmd.Length > 2 )
            {
                errorList.Append("Invalid Syntax at line " + lineNo + "\r\n");
                return;
            }

            else if (cmd.Length == 2) //commands that takes string parameters
            {
                String[] param = cmd[1].Split(',');
                if (cmd[0].Equals("pen"))
                {
                    if (!displayCanvas.setColor(param))
                    {
                        errorList.Append("Invalid pen at line " + lineNo + "\r\n");
                        return;
                    }
                    return;
                }
                if (cmd[0].Equals("fill"))
                {
                    if (!displayCanvas.setFill(param))
                    {
                        errorList.Append("Invalid fill at line " + lineNo + "\r\n");
                        return;
                    }
                    return;
                }



                int[] parameters = new int[param.Length + 2];    //Parameters including x and y cursor coordinates.
                parameters[0] = displayCanvas.x;  
                parameters[1] = displayCanvas.y; 

                //Check for number of parameters for commands.
                if (((cmd[0].Equals("circle")) && parameters.Length != 3)
                || ((cmd[0].Equals("triangle") || cmd[0].Equals("rectangle") || cmd[0].Equals("drawto") || cmd[0].Equals("moveto")) && parameters.Length != 4))     
                {
                    errorList.Append("Invalid parameter numbers at line " + lineNo + "\r\n");
                    return;
                }
                
                for (int i = 2; i < parameters.Length; i++)
                {
                    try // Convert to integer
                    {
                        parameters[i] = int.Parse(param[i - 2]);      
                    }
                    catch (Exception)
                    {
                        errorList.Append("Invalid format at line " + lineNo + "\r\n");
                        return;
                    }
                }

                if (cmd[0].Equals("drawto"))
                {
                    if (!displayCanvas.drawTo(parameters))
                    {
                        errorList.Append("Invalid number of parameters at line " + lineNo + "\r\n");
                        return;
                    }
                    else
                        return;
                }
                if (cmd[0].Equals("moveto"))
                {
                    if (!displayCanvas.moveTo(parameters))
                    {
                        errorList.Append("Invalid number of parameters at line " + lineNo + "\r\n");
                        return;
                    }
                    else
                        return;
                }

                if (cmd[0].Equals("circle") || cmd[0].Equals("rectangle") || cmd[0].Equals("triangle"))
                {
                    ShapeFactory factory = new ShapeFactory();
                    Shape s = (Shape)factory.getShape(cmd[0]);
                    s.set(displayCanvas.color, displayCanvas.fill, parameters);
                    s.draw(displayCanvas.g);
                    return;
                }
                if (cmd[0].Equals("polygon"))
                {
                    int j = 0;
                    for (int i = 2; i < param.Length + 2; i++)
                    {
                        parameters[i] = int.Parse(param[j]);
                        j++;
                    }
                    parameters[0] = displayCanvas.x;
                    parameters[1] = displayCanvas.y;
                    ShapeFactory factory = new ShapeFactory();
                    Shape s = (Shape)factory.getShape(cmd[0]);
                    s.set(displayCanvas.color, displayCanvas.fill, parameters);
                    s.draw(displayCanvas.g);
                    return;
                }
            }
            else
            {
                if (cmd[0].Equals("clear"))
                {
                    displayCanvas.clear();
                    return;
                }
                if (cmd[0].Equals("reset"))
                {
                    displayCanvas.reset();
                    return;
                }
            }
            return;
        }
    }
}
