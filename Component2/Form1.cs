﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Component2
{
    /// <summary>
    /// Form1 inherits the Form class
    /// </summary>
    public partial class Form1 : Form
    {
        CmdParser cmdParser;                  //Parses the command line
        Canvas displayCanvas;                 //canvas for drawing on output bitmap image
        Bitmap drawBoard, cursorBoard;        //output bitmap image, cursor pointer bitmap image
        StringBuilder errors;                 //Stores the list of errors while parsing
        
        /// <summary>
        /// Constructor of Form1 class
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            drawBoard = new Bitmap(410, 499);
            cursorBoard = new Bitmap(410, 499);
            //initializes the canvas objects
            displayCanvas = new Canvas(Graphics.FromImage(drawBoard));
            errors = new StringBuilder();
            drawCursor();
        }

        
        /// <summary>
        /// Draws cursor pointer
        /// </summary>
        public void drawCursor()
        {
            Graphics g = Graphics.FromImage(cursorBoard);
            g.Clear(Color.Transparent);
            g.FillEllipse(new SolidBrush(Color.Red), displayCanvas.x-5, displayCanvas.y-5, 10, 10);
        }


        
        /// <summary>
        /// Checks input from user to run
        /// </summary>
        /// <param name="sender">A reference to object or button which was clicked</param>
        /// <param name="e">An instance of EventArgs class</param>
        private void button1_Click(object sender, EventArgs e)
        {
            String cmd = richTextBox2.Text.Trim().ToLower();
            errors = new StringBuilder();
            if (cmd.Equals("run"))
                run(true);
            else
                run(false);
        }

        /// <summary>
        /// Parses the commands from richtextbox/user input
        /// </summary>
        /// <param name="cmdRun">Boolean value</param>
        public void run(bool cmdRun)
        {
            error.Text = "";
            errors = new StringBuilder();
            cmdParser = new CmdParser();
            cmdParser.parseCommand(richTextBox2,displayCanvas,errors);
            if (!richTextBox1.Text.Equals("") && cmdRun)                   
            {
                cmdParser.parseCommand(richTextBox1, displayCanvas, errors);
                
            }
            drawCursor();
            displayError();
            Refresh();
        }

       

        
        /// <summary>
        /// Display errors found during parsing
        /// </summary>
        public void displayError()
        {
            if(!errors.ToString().Equals(""))
            {
                error.Text = errors.ToString();
            }
            errors.Clear();
        }

        
        /// <summary>
        /// Paint event of picturebox
        /// </summary>
        /// <param name="sender">A reference to object or button which was clicked</param>
        /// <param name="e">An instance of EventArgs class</param>
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(drawBoard, 0,0);
            g.DrawImageUnscaled(cursorBoard, 0,0);
        }

        /// <summary>
        /// Checks syntax of commands
        /// </summary>
        /// <param name="sender">A reference to object or button which was clicked</param>
        /// <param name="e">An instance of EventArgs class</param>
        private void button2_Click(object sender, EventArgs e)
        {
            String cmd = richTextBox2.Text.Trim().ToLower();
            errors = new StringBuilder();
            error.Text = "";
            errors = new StringBuilder();
            cmdParser = new CmdParser();
            cmdParser.parseCommand(richTextBox2, displayCanvas, errors);
            if (!richTextBox1.Text.Equals(""))                   
            {
                cmdParser.parseCommand(richTextBox1, new Canvas(), errors);
            }
            if (richTextBox1.Text.Equals(""))
            {
                error.Text = "Command is Empty";
            }
            if (!richTextBox1.Text.Equals("") && errors.Length == 0)
            {
                error.Text = "No Errors";
            }
            drawCursor();
            displayError();
            Refresh();
        }


        /// <summary>
        /// saves the commands written by the user on the textbox
        /// in a textfile
        /// </summary>
        /// <param name="sender">A reference to object or button which was clicked</param>
        /// <param name="e">An instance of EventArgs class</param>
        private void button3_Click(object sender, EventArgs e)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    // Code to write the stream goes here.
                    StreamWriter txt = new StreamWriter(myStream);
                    txt.Write(richTextBox1.Text);
                    txt.Dispose();
                    myStream.Close();
                }
            }
        }



        /// <summary>
        /// reads and loads text from a textfile onto the textbox.
        /// </summary>
        /// <param name="sender">A reference to object or button which was clicked</param>
        /// <param name="e">An instance of EventArgs class</param>
        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"D:\",
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "txt",
                Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                String file = openFileDialog1.FileName;
                richTextBox1.Text = File.ReadAllText(file);
            }
        }

        
    }
}
