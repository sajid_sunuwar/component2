﻿using System;

namespace Component2
{
    /// <summary>
    /// Get desired shape
    /// </summary>
    public class ShapeFactory
    {
        /// <summary>
        /// Gets shape name for comparison and returns desired shape class
        /// </summary>
        /// <param name="shapeType">Name of shape</param>
        /// <returns>Shape class</returns>
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim();
            if (shapeType == "RECTANGLE")
            { 
                return new Rectangle();
            }
            else if (shapeType == "TRIANGLE")
            {
                return new Triangle();
            }
            else if (shapeType == "CIRCLE")
            {
                return new Circle();
            }
            else if (shapeType == "POLYGON")
            {
                return new Polygon();
            }
            else
            {
                //if we get here then what has been passed in is inkown so throw an appropriate exception
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }
        }
    }
}
