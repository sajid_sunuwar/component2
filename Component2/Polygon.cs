﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Component2
{
    /// <summary>
    /// Polygon class inherits Shape class
    /// </summary>
    public class Polygon : Shape
    {
        int[] pointlist; // Array to store points of polygon.

        /// <summary>
        /// <c>set</c> is overridden to set the parameters of polygon.
        /// </summary>
        /// <param name="color">Colour of polygon</param>
        /// <param name="fill">Boolean to fill shape</param>
        /// <param name="list">List of additional parameters</param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            pointlist = list.Skip(2).ToArray();
        }


        /// <summary>
        /// <c>draw</c> method is overriden to draws/paint the shape of polygon.
        /// </summary>
        /// <param name="g">Graphic object</param>
        public override void draw(Graphics g)
        {
            List<Point> curvePoints = new List<Point>();
            curvePoints.Add(new Point(x, y));
            for (int i = 0; i < pointlist.Length; i += 2)
            {
                try
                {
                    curvePoints.Add(new Point(pointlist[i], pointlist[i + 1]));
                }
                catch
                {
                    Console.WriteLine("error");
                }
            }

            Pen pen = new Pen(this.color, 2);
            g.DrawPolygon(pen, curvePoints.ToArray());


            if (this.fill)
            {
                SolidBrush brush = new SolidBrush(this.color);
                g.FillPolygon(brush, curvePoints.ToArray());
            }

        }
    }
}