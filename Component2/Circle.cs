﻿using System.Drawing;

namespace Component2
{
    /// <summary>
    /// Circle class inherits Shape class
    /// </summary>
    public class Circle : Shape
    {
        /// <summary>
        /// Radius of circle.
        /// </summary>
        public int radius;  

        /// <summary>
        /// <c>set</c> is overridden to set the parameters of circle.
        /// </summary>
        /// <param name="color">Colour of shape</param>
        /// <param name="fill">Boolean to fill shape</param>
        /// <param name="list">List of additional parameters</param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.radius = list[2];
        }


        /// <summary>
        /// <c>draw</c> method is overriden to draws/paint the shape of circle.
        /// </summary>
        /// <param name="g">Graphic object</param>
        public override void draw(Graphics g)
        {
            Pen pen = new Pen(this.color, 2);
            g.DrawEllipse(pen, x - this.radius, y - this.radius, this.radius * 2, this.radius * 2);
            
            if (this.fill)
            {
                SolidBrush brush = new SolidBrush(this.color);

                g.FillEllipse(brush, x - this.radius, y - this.radius, radius * 2, radius * 2);
            }
        }

    }
}
