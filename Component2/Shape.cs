﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2
{
    /// <summary>
    /// <c>Shape</c> is a abstract class which inherits Shapes interface.
    /// </summary>
    public abstract class Shape : Shapes
    {
        /// <summary>
        /// x and y coordinates
        /// </summary>
        protected int x, y;
        /// <summary>
        /// Color of pen and fill
        /// </summary>
        protected Color color;
        /// <summary>
        /// Boolean to set fill on/off
        /// </summary>
        protected bool fill;

        /// <summary>
        /// <c>draw</c> is declared as abstract so that any derived class must implement
        /// this method.
        /// </summary>
        /// <param name="g">Graphics object</param>
        public abstract void draw(Graphics g);


        /// <summary>
        /// <c>Set</c> is declared as virtual so that it can be overidden by child classes.
        /// It sets the parameters of a shape.
        /// </summary>
        /// <param name="color">Shape's colour</param>
        /// <param name="fill">Boolean to fill shape</param>
        /// <param name="list">Additional parameter/variables of shape</param>
        public virtual void set(Color color, bool fill, params int[] list)
        {
            this.color = color; //any shape will have a color of its outline, x axis position, yaxis position   
            this.fill = fill;
            this.x = list[0]; //hence, we are setting these properties here beforehand
            this.y = list[1];// xaxis and yaxis set acc to the general Cartesian rule.
        }
    }
}
