﻿using System.Drawing;

namespace Component2
{
    /// <summary>
    /// Triangle class inherits Shape class
    /// </summary>
    public class Triangle : Shape
    {
        /// <summary>
        /// Height and Width of box triangle drawn in.
        /// </summary>
        public int width, height; 

        /// <summary>
        /// <c>set</c> is overridden to set the parameters of triangle.
        /// </summary>
        /// <param name="color">Colour of shape</param>
        /// <param name="fill">Boolean to fill shape</param>
        /// <param name="list">List of additional parameters</param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }


        /// <summary>
        /// <c>draw</c> method is overriden to draws/paint the shape of triangle.
        /// </summary>
        /// <param name="g">Graphic object</param>
        public override void draw(Graphics g)
        {
            int xPos = x - width / 2;
            int yPos = y + height / 2;
            PointF point = new PointF(xPos, yPos);
            PointF point2 = new PointF(x + width / 2, yPos);
            PointF point3 = new PointF(x, y - height / 2);
            PointF[] polygonPoints = { point, point2, point3 };

            Pen pen = new Pen(this.color, 2);
            g.DrawPolygon(pen, polygonPoints);

            if (this.fill)
            {
                SolidBrush brush = new SolidBrush(this.color);
                g.FillPolygon(brush, polygonPoints);
            }

        }
    }
}
