﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2
{
    /// <summary>
    /// Rectangle class inherits Shape class
    /// </summary>
    public class Rectangle : Shape
    {
        /// <summary>
        /// Width and Height of rectangle
        /// </summary>
        public int width, height;  

        /// <summary>
        /// <c>set</c> is overridden to set the parameters of rectangle.
        /// </summary>
        /// <param name="color">Colour of shape</param>
        /// <param name="fill">Boolean to fill shape</param>
        /// <param name="list">List of additional parameters</param>
        public override void set(Color color, bool fill, params int[] list)
        {   
            base.set(color, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }


        /// <summary>
        /// <c>draw</c> method is overriden to draws/paint the shape of rectangle.
        /// </summary>
        /// <param name="g">Graphic object</param>
        public override void draw(Graphics g)
        {
            Pen pen = new Pen(this.color, 2);
            g.DrawRectangle(pen, x - width/2, y - height/2, width, height);

            if (this.fill)
            {
                SolidBrush brush = new SolidBrush(this.color);
                g.FillRectangle(brush, x - width/2, y - height/2, width, height);
            }

        }

    }
}
