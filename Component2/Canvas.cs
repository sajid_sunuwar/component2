﻿using System;
using System.Drawing;

namespace Component2
{
    /// <summary>
    /// Canvas class 
    /// </summary>
    public class Canvas
    {
        /// <summary>
        /// Object of Graphics
        /// </summary>
        public Graphics g;      
        /// <summary>
        /// x and y coordinates
        /// </summary>
        public int x, y;        
        /// <summary>
        /// Boolean to fill shape
        /// </summary>
        public bool fill;       
        /// <summary>
        /// Color of pen and fill
        /// </summary>
        public Color color;     

        /// <summary>
        /// Constructor of Canvas class
        /// </summary>
        public Canvas()
        {
            Bitmap b = new Bitmap(700, 400);         
            g = Graphics.FromImage(b);
            x = y = 0;
            color = Color.Black;
            fill = false;
        }

        
        /// <summary>
        /// Constructor of Canvas class with parameter
        /// </summary>
        /// <param name="g">Object of Graphics</param>
        public Canvas(Graphics g)
        {
            this.g = g;
            x = y = 0;
            color = Color.Black;
            fill = false;
        }

        
        /// <summary>
        /// Moves the coordinates of cursor
        /// </summary>
        /// <param name="param">x and y coordinates</param>
        /// <returns>Boolean value</returns>
        public bool moveTo(int[] param)
        {
            if (param.Length != 4)
            {
                return false;
            }
            else
            {
                x = param[2];
                y = param[3];
                return true;
            }
        }

        
        /// <summary>
        /// Draws a line to coordinates entered
        /// </summary>
        /// <param name="param">x and y coordinates</param>
        /// <returns>Boolean value</returns>
        public bool drawTo(int[] param)
        {
            if (param.Length != 4)
            {
                return false;
            }
            else
            {
                g.DrawLine(new Pen(color, 2), x, y, param[2], param[3]);
                x = param[2];
                y = param[3];
                return true;
            }   
        }

       
        /// <summary>
        /// Sets color for fill and pen
        /// </summary>
        /// <param name="param">Value of color</param>
        /// <returns>Boolean Value</returns>
        public bool setColor(String[] param)
        {
            param[0] = param[0].Trim().ToLower();       
            if (param.Length != 1)
            {
                return false;
            }

            if (param[0] == "green")
            {
                color = Color.Green;
            }

            if (param[0] == "red")
            {
                color = Color.Red;
            }

            if (param[0] == "yellow")
            {
                color = Color.Yellow;
            }

            if (param[0] == "black")
            {
                color = Color.Black;
            }

            if (param[0] == "blue")
            {
                color = Color.Blue;
            }
                   
            if (param[0] == "orange")
            {
                color = Color.Orange;
            }

            if (param[0] == "silver")
            {
                color = Color.Silver;
            }

            return true;
        }

        
        /// <summary>
        /// Sets fill for shapes
        /// </summary>
        /// <param name="param">String which sets fill on or off</param>
        /// <returns>Boolean Value</returns>
        public bool setFill(String[] param)
        {
            if (param.Length != 1)
            {
                return false;
            }
                
            param[0] = param[0].Trim().ToLower();
            if (param[0].Equals("on"))
            {
                fill = true;
            }
            else if (param[0].Equals("off"))
            {
                fill = false;
            }
            else
            {
                return false;
            }
             
            return true;
        }

        /// <summary>
        /// Clears graphics, resets x and y coordinates, boolean fill to false and color to black
        /// </summary>
        public void reset()
        {
            g.Clear(Color.Silver);
            x = y = 0;
            fill = false;
            color = Color.Black;
        }

        /// <summary>
        /// Clears graphics
        /// </summary>
        public void clear()
        {
            g.Clear(Color.Silver);
        }
    }
}
